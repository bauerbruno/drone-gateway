package View;

public class OutputToConsole implements Output{
    @Override
    public void print(String string) {
        System.out.print(string);
    }

    @Override
    public void print(int number) {
        System.out.print(number);
    }

    @Override
    public void println(String string) {
        System.out.println(string);
    }

    @Override
    public void println(int number) {
        System.out.println(number);
    }
}

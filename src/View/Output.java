package View;

public interface Output {
    public void print(String string);

    public void print(int number);

    public void println(String string);

    public void println(int number);


}

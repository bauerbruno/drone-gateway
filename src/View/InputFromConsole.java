package View;

import java.util.Scanner;

public class InputFromConsole implements  Input{
    Scanner scanner = new Scanner(System.in);
    Output output;

    public InputFromConsole(Output output){
        this.output = output;
    }

    @Override
    public int getInt(String message) {
        output.print(message + ": ");
        return scanner.nextInt();
    }

    @Override
    public String getString(String message) {
        output.print(message + ": ");
        return scanner.next();
    }
}

package Model;

import java.util.ArrayList;
import java.util.Collections;

public class Buffer {
    private ArrayList<ReceivedPosition> buffer = new ArrayList<>();

    public void addPosition(ReceivedPosition position){
        buffer.add(position);
    }

    public ReceivedPosition getPosition(int index){
        return buffer.get(index);
    }

    public int getNumberOfPositions(){
        return buffer.size();
    }

    public void clear(){
        buffer.clear();
    }

    public void delete(int creationId){
        int i = 0;
        while(buffer.get(i).getCreationId() != creationId){
            i++;
        }
        buffer.remove(i);
    }

    public void deleteLastPosition(){
        int indexOfLast = 0;
        for(ReceivedPosition position : buffer){
            if(position.getCreationId() > indexOfLast) indexOfLast = position.getCreationId();
        }
        delete(indexOfLast);
    }

    public void sortBuffer(){
        Collections.sort(buffer);
    }
}

package Model.System;

import Model.Position;
import Model.ReceivedPosition;

public class PositionReceiver {
    private static int lastReceiveId = 0;

    public static ReceivedPosition receive(Position position){
        return new ReceivedPosition(position, ++lastReceiveId);
    }

}

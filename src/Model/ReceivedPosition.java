package Model;

public class ReceivedPosition extends Position implements Comparable<ReceivedPosition>{

    private final int receiveId;

    public ReceivedPosition(Position position, int receiveId) {
        super(
                position.getCoordinateX(),
                position.getCoordinateY(),
                position.getCoordinateZ(),
                position.getCreationId()
        );
        this.receiveId = receiveId;
    }

    public int getReceiveId() {
        return receiveId;
    }

    @Override
    public int compareTo(ReceivedPosition o) {
        return o.receiveId - receiveId;
    }

}

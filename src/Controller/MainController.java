package Controller;

import Model.GateWay;
import Model.NetworkStorage;
import View.*;

public class MainController {
    Output output = new OutputToConsole();
    Input input = new InputFromConsole(output);
    Drone consoleDrone = new ConsoleDrone(input);
    NetworkStorage networkStorage = new NetworkStorage(output);
    GateWay gateWay = new GateWay(networkStorage, output);
    GateWayController gateWayController = new GateWayController(gateWay, output, input);

    public void main(){
        try{
            executeOptions();
        }catch (Exception e){
            output.println("Something went wrong");
            output.println(e.toString());
        }
    }

    private boolean executeOption(int option){
        switch (option){
            case 1 :
                gateWayController.receivePositionFromDrone(consoleDrone);
                break;
            case 2 :
                gateWayController.listBuffer();
                break;
            case 3 :
                gateWayController.deleteLastPositions();
                break;
            case 4 :
                gateWayController.checkConnection();
                break;
            case 5 :
                gateWayController.changeConnection();
                break;
            case 6 :
                return false;
        }
        return true;
    }

    private void executeOptions() throws Exception{
        boolean again;
        do {
            showOptions();
            again = executeOption(optionInput());
        }while(again);
    }

    private void showOptions(){
        output.println("----------Options----------");
        output.println("1 - New position");
        output.println("2 - List buffer");
        output.println("3 - Delete last positions from buffer");
        output.println("4 - Check connection");
        output.println("5 - Change connection");
        output.println("6 - Exit");
        output.println("");
    }

    private int optionInput(){
        int option;
        do{
           option = input.getInt("Option (1 - 6)");
        }while(option < 1 || option > 6);
        return option;
    }
}

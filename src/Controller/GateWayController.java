package Controller;

import Model.Buffer;
import Model.GateWay;
import Model.Position;
import View.Drone;
import View.Input;
import View.Output;

public class GateWayController {
    private GateWay gateWay;
    private Output output;
    private Input input;

    public GateWayController(GateWay gateWay, Output output, Input input){
        this.gateWay = gateWay;
        this.output  = output;
        this.input = input;
    }

    public void listBuffer(){
        output.println("Listing buffer ");
        Buffer buffer = gateWay.getBuffer();
        if(buffer.getNumberOfPositions() == 0){
            output.println("Empty");
        }else{
            buffer.sortBuffer();
            for (int i = 0; i < buffer.getNumberOfPositions(); i++){
                output.println(buffer.getPosition(i).toString());
            }
        }

    }

    public void receivePositionFromDrone(Drone drone){
        Position position = drone.sendPosition();
        gateWay.receivePosition(position);
        if(gateWay.storageIsOnline()){
            output.println(position.toString());
        }else{
            output.println(Integer.toString(gateWay.getBuffer().getNumberOfPositions()) + " data in buffer");
        }
    }

    public void changeGateWay(GateWay gateWay){
        this.gateWay = gateWay;
    }

    public void deleteLastPositions(){
        gateWay.deleteLastPositionsFromBuffer(
                input.getInt("Number of positions to delete")
        );
    }

    public void checkConnection(){
        if(gateWay.storageIsOnline()){
            output.println("Network Storage is online");
        } else {
            output.println("Network Storage is offline");
        }
    }

    public void changeConnection(){
        checkConnection();
        String stringInput;
        do{
            stringInput = input.getString("Change connection to(online/offline)");
        }while(!stringInput.equals("online") && !stringInput.equals("offline"));
        if(stringInput.equals("online")){
            listBuffer();
            gateWay.setStorageToOnline();
        }else{
            gateWay.setStorageToOffline();
        }
        checkConnection();
    }


}
